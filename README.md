 
<h1> Desafio Bootcamp - AVANTI</h1>

O intuito do desafio é criar a infraestrutura com terraform na AWS, criar um deploy com o GitLab, na EC2 na AWS. Porém por falta de Recursos, estarei fazendo o desafio usando Vagrant e VirtualBox

#Processo

Subi uma Maquina virtual ubuntu usando Vagrant com as seguintes Configurações

```ruby
Vagrant.configure("2") do |config|


  config.vm.box = "ubuntu/focal64"

    config.vm.network "public_network"

    config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
      vb.gui = false
  #
  #   # Customize the amount of memory on the VM:
      vb.memory = "1024"
      vb.cpus = "2"
    end
    config.vm.boot_timeout = 5000

    config.vm.provision "shell", path: "script.sh"

end
```
Agora o Script que prepara o ambiente no servidor, aonde ele baixa o pacote do gitlab-runner e faz o registro com o token

```bash
sudo apt update
sudo apt upgrade -y
sudo apt install apache2 -y

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt install gitlab-runner -y

sudo chown gitlab-runner /var/www/html/ -R

sudo gitlab-runner register \
--non-interactive \
--url https://gitlab.com/ \
--registration-token AQUIFICAOTOKENDOPIPELINE \
--executor shell \
--description "Web Server" \
--tag-list "AWS,Apache"

ip a
```
Project Runner Funcionando

<img src="./img/runner.png">


Site funcionando

<img src="./img/htmlup.png">
